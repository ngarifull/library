**Instructions to start "Library" application**
---

## Database and RabbitMQ

1. cd /project_path/infrastructure
2. docker-compose up -d

---

## Project settings

0. Install Java11
1. Open application.yml
2. Set at least JWT_TOKEN_SECRET to Environment 
3. Application will start on 8080 port.

---

This is pet project. if you have any suggestions, tell me on telegram : @ngariful