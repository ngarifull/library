CREATE TABLE job
(
    id             BIGSERIAL PRIMARY KEY,
    name           VARCHAR(256),
    description    VARCHAR(2000),
    code           VARCHAR(256),
    cron           varchar(2000),
    execution_time INT,
    once_per_day   NUMERIC(1, 0) NOT NULL DEFAULT 0 check ( is_deleted in (0, 1) ),
    is_deleted     NUMERIC(1, 0) NOT NULL DEFAULT 0 check ( is_deleted in (0, 1) )
);


CREATE TABLE job_history
(
    id       BIGSERIAL PRIMARY KEY,
    result   VARCHAR(2000),
    started  TIMESTAMP(6) not null,
    finished TIMESTAMP(6),
    job_id   BIGINT,
    FOREIGN KEY (job_id) REFERENCES job (id)
);
