CREATE TABLE authors (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL ,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    is_deleted NUMERIC(1, 0) NOT NULL DEFAULT 0
);

ALTER TABLE book ADD COLUMN author_id BIGINT, ADD FOREIGN KEY (author_id) REFERENCES authors(id);

INSERT INTO authors(name) VALUES ('Gregory David');
INSERT INTO authors(name) VALUES ('Joanne Rowling');
INSERT INTO authors(name) VALUES ('Джордж Р. Р. Мартин');
INSERT INTO authors(name) VALUES ('Paulo Coelho');
INSERT INTO authors(name) VALUES ('Harper Lee');
INSERT INTO authors(name) VALUES ('Peter Thiel');

UPDATE book SET author_id = 1 WHERE id = 1;
UPDATE book SET author_id = 2 WHERE id = 2;
UPDATE book SET author_id = 3 WHERE id = 3;
UPDATE book SET author_id = 4 WHERE id = 4;
UPDATE book SET author_id = 5 WHERE id = 5;
UPDATE book SET author_id = 6 WHERE id = 6;