CREATE TABLE genre (
   id BIGINT PRIMARY KEY,
   name VARCHAR(255) NOT NULL,
   description VARCHAR(2048),
   created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   is_deleted NUMERIC(1, 0) NOT NULL DEFAULT 0 check ( is_deleted in (0,1))
);

ALTER TABLE book
    ADD genre_id BIGINT,
    ADD FOREIGN KEY (genre_id) REFERENCES genre(id);

INSERT INTO genre VALUES(1,'Crime', 'crime description',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,0);
INSERT INTO genre VALUES(2,'Drama', 'drama description',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,0);
INSERT INTO genre VALUES(3,'Fantasy', 'fantasy description',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,0);
INSERT INTO genre VALUES(4,'Fairytale', 'fairytale description',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,0);
INSERT INTO genre VALUES(5,'Business', 'business description',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,0);

INSERT INTO book (id, title, status, genre_id, description) VALUES (3, 'Shantaram', 'AVAILABLE', 4, 'Shantaram is a 2003 novel by Gregory David Roberts, in which a convicted Australian bank robber and heroin addict who escaped from Pentridge Prison flees to India. The novel is commended by many for its vivid portrayal of tumultuous life in Bombay.');
INSERT INTO book (id, title, status, genre_id, description) VALUES (1, 'Harry Potter', 'AVAILABLE', 3, 'Harry James Potter is the titular protagonist of J. K. Rowling''s Harry Potter series. The majority of the books'' plot covers seven years in the life of the orphan Potter, who, on his eleventh birthday, learns he is a wizard. Thus, he attends Hogwarts School of Witchcraft and Wizardry to practice magic under the guidance of the kindly headmaster Albus Dumbledore and other school professors along with his best friends Ron Weasley and Hermione Granger. Harry also discovers that he is already famous throughout the novel''s magical community, and that his fate is tied with that of Lord Voldemort, the internationally feared Dark Wizard and murderer of his parents, Lily and James. The film and book series revolve around Harry''s struggle to adapt to the wizarding world and defeat Voldemort. Harry is considered a fictional icon and has been described by many critics, readers, and audiences as one of the greatest literary and film characters of all time.');
INSERT INTO book (id, title, status, genre_id, description) VALUES (4, 'Игра престолов (роман)', 'AVAILABLE', 3, '«Игра престолов» (англ. A Game of Thrones) — роман в жанре темное фэнтези американского писателя Джорджа Р. Р. Мартина, первая книга из серии «Песнь Льда и Огня». Впервые произведение было опубликовано в 1996 году издательством Bantam Spectra. Действие романа происходит в вымышленной вселенной. В центре произведения три основные сюжетные линии — события, предшествующие началу династических войн за власть над континентом Вестерос, напоминающим Европу времён Высокого Средневековья; надвигающаяся угроза наступления племён одичалых и демонической расы Иных; а также путешествие дочери свергнутого короля в попытках вернуть Железный трон. Повествование ведётся от третьего лица, попеременно с точки зрения разных персонажей.');
INSERT INTO book (id, title, status, genre_id, description) VALUES (5, 'The Alchemist (novel)', 'AVAILABLE', 2, 'The Alchemist (Portuguese: O Alquimista) is a novel by Brazilian author Paulo Coelho that was first published in 1988. Originally written in Portuguese');
INSERT INTO book (id, title, status, genre_id, description) VALUES (6, 'To Kill a Mockingbird', 'AVAILABLE', 1, 'To Kill a Mockingbird is a novel by Harper Lee published in 1960. Instantly successful');
INSERT INTO book (id, title, status, genre_id, description) VALUES (2, 'Zero to one', 'AVAILABLE', 5, 'Zero to One: Notes on Startups, or How to Build the Future is a 2014 book by venture capitalist, PayPal co-founder, and early Facebook investor Peter Thiel along with Blake Masters. It is a condensed and updated version of a highly popular set of online notes taken by Masters for the CS183 class on startups taught by Thiel at Stanford University in Spring 2012.');
