CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(255) UNIQUE NOT NULL ,
    firstName VARCHAR(255) NOT NULL ,
    lastName VARCHAR(255),
    email VARCHAR(255) UNIQUE NOT NULL ,
    password VARCHAR(255) NOT NULL ,
    status VARCHAR(255) NOT NULL DEFAULT 'ACTIVE',
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    is_deleted NUMERIC(1, 0) NOT NULL DEFAULT 0
);

CREATE TABLE roles(
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL ,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    is_deleted NUMERIC(1, 0) NOT NULL DEFAULT 0
);

CREATE TABLE user_roles(
    user_id BIGINT,
    role_id BIGINT,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (role_id) REFERENCES roles(id)
);

INSERT INTO roles(name) VALUES ('ROLE_ADMIN');
INSERT INTO roles(name) VALUES ('ROLE_USER');

INSERT INTO users(username, firstName, email, password, status) VALUES ('admin', 'admin', 'admin@gmail.com', '$2a$04$mbanIS5TjI1H1EZ04.GO1.6R3kRyFcHYRNkmIGsL/N8Y1pyGs1uY2', 'ACTIVE');
INSERT INTO users(username, firstName, email, password, status) VALUES ('user', 'user', 'user@gmail.com', '$2a$04$mbanIS5TjI1H1EZ04.GO1.6R3kRyFcHYRNkmIGsL/N8Y1pyGs1uY2', 'ACTIVE');

INSERT INTO user_roles VALUES (1,1);
INSERT INTO user_roles VALUES (1,2);
INSERT INTO user_roles VALUES (2,2);