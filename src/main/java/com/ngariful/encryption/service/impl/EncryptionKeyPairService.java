package com.ngariful.encryption.service.impl;

import com.ngariful.encryption.model.EncryptionKeyPair;
import com.ngariful.encryption.model.dto.PublicKeyDto;
import com.ngariful.encryption.repository.EncryptionKeyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class EncryptionKeyPairService {
    private final EncryptionKeyRepository encryptionKeyRepository;

    @Cacheable("encryptionKeyPair")
    public EncryptionKeyPair getRsaKeysPair(){
        return encryptionKeyRepository.getEncryptionKey();
    }

    @Cacheable("publicKey")
    public PublicKeyDto getPublicKey(){
        EncryptionKeyPair encryptionKeyPair = getRsaKeysPair();
        return PublicKeyDto.of(encryptionKeyPair.getPublicKey());
    }
}
