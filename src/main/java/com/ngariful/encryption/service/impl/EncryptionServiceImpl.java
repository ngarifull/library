package com.ngariful.encryption.service.impl;

import com.ngariful.encryption.model.EncryptionKeyPair;
import com.ngariful.encryption.model.dto.PublicKeyDto;
import com.ngariful.encryption.repository.EncryptionKeyRepository;
import com.ngariful.encryption.service.EncryptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.transaction.Transactional;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.LocalDateTime;
import java.util.Base64;

@Service
@RequiredArgsConstructor
public class EncryptionServiceImpl implements EncryptionService {
    private final EncryptionKeyPairService encryptionKeyPairService;
    private final EncryptionKeyRepository encryptionKeyRepository;

    @Override
    public String decrypt(String data) throws Exception {
        return decrypt(Base64.getDecoder().decode(data.getBytes()), getPrivateKey());
    }

    @Override
    @Transactional
    public void archiveOldKeys() {
        encryptionKeyRepository.archiveOldKeys(LocalDateTime.now());
    }

    @Override
    @Transactional
    public void save(EncryptionKeyPair encryptionKeyPair) {
        encryptionKeyRepository.save(encryptionKeyPair);
    }

    @Override
    public PublicKeyDto getPublicKey() {
        return encryptionKeyPairService.getPublicKey();
    }

    @Override
    public EncryptionKeyPair generateEncryptionKeyPair() throws NoSuchAlgorithmException {
        KeyPair keyPair = generateKeyPair();
        String publicKey = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
        String privateKey = Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded());

        EncryptionKeyPair encryptionKeyPair = new EncryptionKeyPair();
        encryptionKeyPair.setPrivateKey(privateKey);
        encryptionKeyPair.setPublicKey(publicKey);

        return encryptionKeyPair;
    }

    private KeyPair generateKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        return keyGen.generateKeyPair();
    }

    private String decrypt(byte[] data, PrivateKey privateKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(cipher.doFinal(data));
    }

    private PrivateKey getPrivateKey(){
        PrivateKey privateKey = null;
        EncryptionKeyPair encryptionKeyPair = encryptionKeyPairService.getRsaKeysPair();
        String base64PrivateKey = encryptionKeyPair.getPrivateKey();
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            privateKey = keyFactory.generatePrivate(keySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return privateKey;
    }


}