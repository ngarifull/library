package com.ngariful.encryption.service;

import com.ngariful.encryption.model.EncryptionKeyPair;
import com.ngariful.encryption.model.dto.PublicKeyDto;

import java.security.NoSuchAlgorithmException;

public interface EncryptionService {
    String decrypt(String data) throws Exception;
    void archiveOldKeys();
    EncryptionKeyPair generateEncryptionKeyPair() throws NoSuchAlgorithmException;
    void save(EncryptionKeyPair encryptionKeyPair);
    PublicKeyDto getPublicKey();
}
