package com.ngariful.encryption.model;

import com.ngariful.authentication.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "encryption_keys_store")
public class EncryptionKeyPair extends BaseEntity {
    private Long id;
    private String publicKey;
    private String privateKey;
}
