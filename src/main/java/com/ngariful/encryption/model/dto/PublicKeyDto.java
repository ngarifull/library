package com.ngariful.encryption.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@AllArgsConstructor
public class PublicKeyDto {
    private String publicKey;

    public static PublicKeyDto of(String publicKey){
        return new PublicKeyDto(publicKey);
    }
}
