package com.ngariful.encryption.repository;

import com.ngariful.encryption.model.EncryptionKeyPair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

public interface EncryptionKeyRepository extends JpaRepository<EncryptionKeyPair, Long> {

    @Query("SELECT ekp FROM EncryptionKeyPair ekp WHERE ekp.isDeleted = 0 ORDER BY ekp.created DESC")
    EncryptionKeyPair getEncryptionKey();

    @Modifying
    @Query("UPDATE EncryptionKeyPair ekp SET ekp.isDeleted = 1 WHERE ekp.created < :now ")
    void archiveOldKeys(@Param("now") LocalDateTime now);
}
