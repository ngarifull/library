package com.ngariful.encryption.controller;

import com.ngariful.constants.Constants;
import com.ngariful.encryption.model.dto.PublicKeyDto;
import com.ngariful.encryption.service.EncryptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = Constants.API_BASE + "/public-key")
public class EncryptionController {
    private final EncryptionService encryptionService;

    @GetMapping
    public ResponseEntity<PublicKeyDto> getPublicKey(){
        PublicKeyDto publicKeyDto = encryptionService.getPublicKey();
        return ResponseEntity.ok(publicKeyDto);
    }
}
