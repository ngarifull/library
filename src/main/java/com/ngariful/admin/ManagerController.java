package com.ngariful.admin;


import com.ngariful.constants.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = Constants.API_BASE)
public class ManagerController {

    @GetMapping("/admin")
    public String admin() {
        return "Hi! you are admin!";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/testing1")
    public String testing1() {
        return "Hi! you are admin!";
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/testing2")
    public String testing2() {
        return "Hi! you are admin!";
    }
}
