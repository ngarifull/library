package com.ngariful.book.controller;

import com.ngariful.book.model.Book;
import com.ngariful.book.model.BookDto;
import com.ngariful.constants.Constants;
import com.ngariful.book.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = Constants.API_BASE + "/books")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @GetMapping
    public List<BookDto> getBooks(){
        return bookService.getAllBooks();
    }

    @GetMapping(path = "/{id}")
    public Book getBook(@PathVariable long id){
        return bookService.getBook(id);
    }

    @PutMapping
    public void changeBookStatus(@RequestBody Book book){
        bookService.updateBook(book.getId(), book.getStatus());
    }

    @GetMapping(path = "genre/{genreId}")
    public List<BookDto> getBooksByGenre(@PathVariable long genreId){
        return bookService.getBooksByGenre(genreId);
    }

    @GetMapping(path = "author/{auhtorId}")
    public List<BookDto> getBooksByAuthor(@PathVariable long auhtorId){
        return bookService.getBooksByAuthor(auhtorId);
    }
}
