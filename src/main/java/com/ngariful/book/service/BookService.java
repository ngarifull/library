package com.ngariful.book.service;

import com.ngariful.book.model.Book;
import com.ngariful.book.model.BookDto;
import com.ngariful.book.model.BookStatus;
import com.ngariful.book.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BookService {
    private final BookRepository bookRepository;

    public List<BookDto> getAllBooks(){
        List<Book> bookList = bookRepository.findAll();
        return bookList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    public Book getBook(long id){
        return bookRepository.findById(id);
    }

    public List<BookDto> getBooksByGenre(long genreId){
        List<Book> bookList = bookRepository.findAllByGenre_Id(genreId);
        return bookList.stream()
                .map(book -> convertToDto(book))
                .collect(Collectors.toList());
    }

    public List<BookDto> getBooksByAuthor(long genreId){
        List<Book> bookList = bookRepository.findAllByAuthor_Id(genreId);
        return bookList.stream()
                .map(book -> convertToDto(book))
                .collect(Collectors.toList());
    }

    public void updateBook(long id, BookStatus status){
        bookRepository.changeStatus(id, status);
    }

    private BookDto convertToDto(Book book){
        return BookDto.builder()
                .id(book.getId())
                .author(book.getAuthor().getName())
                .description(book.getDescription())
                .title(book.getTitle())
                .genre(book.getGenre().getName())
                .status(book.getStatus())
                .build();
    }
}
