package com.ngariful.book.repository;

import com.ngariful.book.model.Book;
import com.ngariful.book.model.BookStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface BookRepository extends JpaRepository<Book, Long> {
    @Query("SELECT b FROM Book b ORDER BY  b.id")
    List<Book> findAll();

    @Query("SELECT b FROM Book b WHERE b.id = :id")
    Book findById(@Param("id") long id);

    @Modifying
    @Query("UPDATE Book b SET b.status = :status WHERE b.id = :id")
    void changeStatus(@Param("id") long id, @Param("status") BookStatus status);

    List<Book> findAllByGenre_Id(long genre_id);

    List<Book> findAllByAuthor_Id(long author_id);
}
