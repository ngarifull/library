package com.ngariful.book.model;
import com.ngariful.author.model.Author;
import com.ngariful.genre.model.Genre;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Data
@Table(name = "book")
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String title;

    @Enumerated(EnumType.STRING)
    private BookStatus status;

    @ManyToOne
    @JoinColumn(name = "genre_id")
    private Genre genre;

    @ManyToOne
    @JoinColumn(name="author_id")
    private Author author;

    private String description;

    public Book() {}

    public Book(String name) {
        this.title = name;
    }

}
