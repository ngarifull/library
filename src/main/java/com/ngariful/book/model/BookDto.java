package com.ngariful.book.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class BookDto {
    final private long id;
    final private String title;
    final private String author;
    final private String description;
    final private String genre;
    final private BookStatus status;
}
