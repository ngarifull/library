package com.ngariful.book.model;

public enum BookStatus {
    AVAILABLE, NOT_AVAILABLE
}
