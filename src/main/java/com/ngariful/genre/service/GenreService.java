package com.ngariful.genre.service;

import com.ngariful.genre.dto.GenreDto;
import com.ngariful.genre.model.Genre;
import com.ngariful.genre.repository.GenreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class GenreService {
    private final GenreRepository genreRepository;

    public List<GenreDto> getGenreList(){

        List<Genre> genreList = genreRepository.findAll();
        return  genreList.stream()
                .map(genre -> convertToDto(genre))
                .collect(Collectors.toList());
    }

    public void updateGenre(Genre genre){
        genreRepository.save(genre);
    }

    public Genre getGenre(long id){
        return genreRepository.findById(id).orElse(null);
    }

    private GenreDto convertToDto(Genre genre) {
        GenreDto genreDto = GenreDto.builder()
                                .id(genre.getId())
                                .name(genre.getName())
                                .build();
        return genreDto;
    }
}
