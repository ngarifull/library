package com.ngariful.genre.controller;

import com.ngariful.constants.Constants;
import com.ngariful.genre.dto.GenreDto;
import com.ngariful.genre.model.Genre;
import com.ngariful.genre.service.GenreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = Constants.API_BASE + "/genres")
@RequiredArgsConstructor
public class GenreController {
    private final GenreService genreService;

    @GetMapping
    public List<GenreDto> getGenreList(){
        return genreService.getGenreList();
    }

    @GetMapping(path = "{id}")
    public Genre getBook(@PathVariable long id){
        return genreService.getGenre(id);
    }

    @PutMapping
    public void changeBookStatus(@RequestBody Genre genre){
        genreService.updateGenre(genre);
    }

}
