package com.ngariful.genre.model;


import com.ngariful.authentication.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "genre")
public class Genre extends BaseEntity {
    private String name;
    private String description;

    public Genre() {}
}
