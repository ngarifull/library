package com.ngariful.genre.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class GenreDto {
    Long id;
    String name;
}
