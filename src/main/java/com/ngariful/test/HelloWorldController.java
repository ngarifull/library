package com.ngariful.test;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class HelloWorldController {
    private final ApplicationContext applicationContext;

    @GetMapping("/hello")
    public void hello() {
        List<String> stringList = Arrays.asList(applicationContext.getBeanDefinitionNames());

        stringList.forEach(beanName -> {
            System.out.println(beanName + " : " + applicationContext.getBean(beanName).getClass().toString());
        });
    }
}