package com.ngariful.author.controller;

import com.ngariful.author.service.AuthorService;
import com.ngariful.author.dto.AuthorDto;
import com.ngariful.author.model.Author;
import com.ngariful.constants.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = Constants.API_BASE + "/authors")
public class AuthorController {
    private final AuthorService authorService;

    @GetMapping
    public List<AuthorDto> getAuthorList(){
        return authorService.getAuhtorList();
    }

    @RolesAllowed("ADMIN")
    @GetMapping(path = "/{id}")
    public Author getAuthor(@PathVariable long id){
        return authorService.getAuthor(id);
    }

    @PutMapping
    public void changeAuthor(@RequestBody Author author){
        authorService.updateAuthor(author);
    }

}
