package com.ngariful.author.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AuthorDto {
    private long id;
    private String name;
}
