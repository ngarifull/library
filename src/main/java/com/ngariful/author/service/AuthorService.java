package com.ngariful.author.service;

import com.ngariful.author.dto.AuthorDto;
import com.ngariful.author.model.Author;
import com.ngariful.author.repository.AuthorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class AuthorService {
    private final AuthorRepository authorRepository;

    public List<AuthorDto> getAuhtorList(){
        List<Author> authorList= authorRepository.findAll();
        return authorList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    public Author getAuthor(long id){
        return authorRepository.findById(id).orElse(null);
    }

    public void updateAuthor(Author author){
        authorRepository.save(author);
    }

    private AuthorDto convertToDto(Author author) {
        AuthorDto authorDTO = AuthorDto.builder()
                .id(author.getId())
                .name(author.getName())
                .build();
        return authorDTO;
    }
}
