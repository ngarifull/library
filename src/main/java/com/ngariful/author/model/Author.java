package com.ngariful.author.model;

import com.ngariful.authentication.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "authors")
@EqualsAndHashCode(callSuper = true)
public class Author extends BaseEntity {
    private String name;
}
