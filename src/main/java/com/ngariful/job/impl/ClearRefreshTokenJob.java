package com.ngariful.job.impl;

import com.ngariful.authentication.service.RefreshTokenStoreService;
//import com.ngariful.model.SimpleJob;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClearRefreshTokenJob {
//        implements SimpleJob {
    private final RefreshTokenStoreService refreshTokenStoreService;

    private static final String JOB_CODE = "CLEAR_REFRESH_TOKEN_JOB";

//    @Override
    public void run() {
        log.info("clear refresh token job started");
        refreshTokenStoreService.clearRefreshTokens();
    }

//    @Override
    public boolean supports(String code) {
        return JOB_CODE.equals(code);
    }

}
