package com.ngariful.job.impl;

import com.ngariful.encryption.model.EncryptionKeyPair;
//import com.ngariful.model.SimpleJob;
import com.ngariful.encryption.service.EncryptionService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class GenerateNewEncryptionKeysJob {
//        implements SimpleJob {
    private final EncryptionService encryptionService;

    private static final String JOB_CODE = "CLEAR_REFRESH_TOKEN_JOB";

//    @Override
    @SneakyThrows
    @Transactional
    @Caching(evict = { @CacheEvict("encryptionKeyPair"), @CacheEvict("publicKey") })
    public void run() {
        log.info("Generating new public/private key pair");

        encryptionService.archiveOldKeys();
        EncryptionKeyPair encryptionKeyPair = encryptionService.generateEncryptionKeyPair();
        encryptionService.save(encryptionKeyPair);
    }

//    @Override
    public boolean supports(String code) {
        return JOB_CODE.equals(code);
    }
}
