package com.ngariful.job.impl;

//import com.ngariful.model.SimpleJob;
import com.ngariful.authentication.service.RefreshTokenStoreService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class DeleteOutdatedRefreshTokenJob {
//        implements SimpleJob {
    private final RefreshTokenStoreService refreshTokenStoreService;

    private static final String JOB_CODE = "DELETE_OUTDATED_REFRESH_TOKEN_JOB";

//    @Override
    public void run() {
        log.info("delete refresh token job started");
        refreshTokenStoreService.deleteOutdatedTokens();
    }

//    @Override
    public boolean supports(String code) {
        return JOB_CODE.equals(code);
    }

}
