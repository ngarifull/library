package com.ngariful.authentication.service;

import com.ngariful.authentication.model.RefreshTokenStore;

public interface RefreshTokenStoreService {
    RefreshTokenStore get(String refreshToken);
    void save(RefreshTokenStore refreshTokenStore);
    void archive(RefreshTokenStore refreshTokenStore);
    void clearRefreshTokens();
    void deleteOutdatedTokens();
}
