package com.ngariful.authentication.service;

import com.ngariful.authentication.model.dto.RefreshTokenRequestDto;
import com.ngariful.authentication.model.dto.TokenResponseDto;
import com.ngariful.authentication.model.dto.AuthenticationRequestDto;

public interface AuthenticationService {
    TokenResponseDto login(AuthenticationRequestDto requestDto) throws Exception;
    TokenResponseDto refresh(RefreshTokenRequestDto data) throws Exception;
}
