package com.ngariful.authentication.service;

import com.ngariful.authentication.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User register(User user);

    List<User> getAll();

    Optional<User> findByUsername(String username);

    User findById(Long id);

    void delete(Long id);
}
