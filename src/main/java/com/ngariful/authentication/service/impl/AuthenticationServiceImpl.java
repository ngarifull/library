package com.ngariful.authentication.service.impl;

import com.ngariful.authentication.middleware.RefreshTokenVerificationChain;
import com.ngariful.authentication.model.RefreshTokenStore;
import com.ngariful.authentication.model.UserStatus;
import com.ngariful.authentication.model.dto.RefreshTokenRequestDto;
import com.ngariful.authentication.model.dto.TokenResponseDto;
import com.ngariful.authentication.model.User;
import com.ngariful.authentication.model.dto.AuthenticationRequestDto;
import com.ngariful.authentication.security.jwt.JwtTokenProvider;
import com.ngariful.authentication.service.AuthenticationService;
import com.ngariful.authentication.service.RefreshTokenStoreService;
import com.ngariful.authentication.service.UserService;
import com.ngariful.encryption.service.EncryptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;
    private final RefreshTokenStoreService refreshTokenStoreService;
    private final RefreshTokenVerificationChain refreshTokenVerificationChain;
    private final EncryptionService encryptionService;

    @Override
    public TokenResponseDto login(AuthenticationRequestDto requestDto) throws Exception {
        String username = encryptionService.decrypt(requestDto.getUsername());
        String password = encryptionService.decrypt(requestDto.getPassword());

        User user = userService.findByUsername(username).orElseThrow(() ->
                new UsernameNotFoundException(String.format("User with username: %s not found", username)));

        if (UserStatus.isDisabled(user.getStatus())) {
            throw new LockedException(String.format("User with username: %s is locked", username));
        }

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        return generateTokenResponse(user);
    }

    @Override
    // add corresponding exceptions later
    public TokenResponseDto refresh(RefreshTokenRequestDto data) throws Exception {
        refreshTokenVerificationChain.apply(data);

        String username = data.getUsername();
        User user = userService.findByUsername(username).orElseThrow(() ->
                new UsernameNotFoundException(String.format("User with username: %s not found", username)));

        return generateTokenResponse(user);
    }

    private TokenResponseDto generateTokenResponse(User user) {
        String accessToken = jwtTokenProvider.createToken(user.getUsername(), user.getRoles());
        UUID refreshToken = UUID.randomUUID();

        TokenResponseDto response = TokenResponseDto.builder()
                .username(user.getUsername())
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();

        RefreshTokenStore refreshTokenStore = RefreshTokenStore.of(refreshToken.toString(), user.getId());
        refreshTokenStoreService.save(refreshTokenStore);

        return response;
    }
}
