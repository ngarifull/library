package com.ngariful.authentication.service.impl;

import com.ngariful.authentication.model.RefreshTokenStore;
import com.ngariful.authentication.repository.RefreshTokenStoreRepository;
import com.ngariful.constants.Constants;
import com.ngariful.authentication.service.RefreshTokenStoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Service
@RequiredArgsConstructor
public class RefreshTokenStoreServiceImpl implements RefreshTokenStoreService {
    private final RefreshTokenStoreRepository repository;

    @Value("${jwt.token.refresh.minutes}")
    private long REFRESH_TOKEN_MINUTES;

    @Override
    public RefreshTokenStore get(String refreshToken) {
        return repository.getByRefreshToken(refreshToken);
    }

    @Override
    public void save(RefreshTokenStore refreshTokenStore) {
        refreshTokenStore.setExpires(LocalDateTime.now().plusMinutes(REFRESH_TOKEN_MINUTES));
        repository.save(refreshTokenStore);
    }

    @Override
    public void archive(RefreshTokenStore refreshTokenStore) {
        refreshTokenStore.setIsDeleted(Constants.TRUE);
        repository.save(refreshTokenStore);
    }

    @Override
    @Transactional
    public void clearRefreshTokens() {
        LocalDateTime clearTime = LocalDateTime.of(LocalDate.now().minusDays(7), LocalTime.MIN);
        repository.clearExpired(clearTime);
    }

    @Override
    @Transactional
    public void deleteOutdatedTokens() {
        LocalDateTime deleteTime = LocalDateTime.now().minusMinutes(REFRESH_TOKEN_MINUTES);
        repository.deleteExpired(deleteTime);
    }
}
