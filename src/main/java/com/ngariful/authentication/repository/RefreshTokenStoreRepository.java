package com.ngariful.authentication.repository;

import com.ngariful.authentication.model.RefreshTokenStore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface RefreshTokenStoreRepository extends JpaRepository<RefreshTokenStore, Long> {
    @Query("SELECT rts FROM RefreshTokenStore rts WHERE rts.refreshToken = :refreshToken AND rts.isDeleted = 0")
    RefreshTokenStore getByRefreshToken(@Param("refreshToken") String refreshToken);

    @Modifying
    @Query("UPDATE RefreshTokenStore rts SET rts.isDeleted = 1 WHERE rts.expires < :deleteTime")
    void deleteExpired(@Param("deleteTime") LocalDateTime deleteTime);

    @Modifying
    @Query("DELETE FROM RefreshTokenStore rts WHERE rts.expires < :clearTime")
    void clearExpired(@Param("clearTime") LocalDateTime clearTime);
}
