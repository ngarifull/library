package com.ngariful.authentication.model;

import java.util.ArrayList;
import java.util.List;

public enum UserStatus {
    ACTIVE, NOT_ACTIVE, DELETED;

    public static boolean isDisabled(UserStatus status){
        List<UserStatus> disabledStatuses = new ArrayList<>();
        disabledStatuses.add(NOT_ACTIVE);
        disabledStatuses.add(DELETED);

        return disabledStatuses.contains(status);
    }
}
