package com.ngariful.authentication.model.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

@EqualsAndHashCode
@Data
@Builder
public class TokenResponseDto {
    private UUID refreshToken;
    private String username;
    private String accessToken;
}
