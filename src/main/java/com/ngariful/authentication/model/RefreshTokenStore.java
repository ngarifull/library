package com.ngariful.authentication.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "refresh_token_store")
public class RefreshTokenStore extends BaseEntity {
    private Long userId;
    private String refreshToken;
    private LocalDateTime expires;

    public static RefreshTokenStore of(String refreshToken, Long userId) {
        RefreshTokenStore refreshTokenStore = new RefreshTokenStore();
        refreshTokenStore.setRefreshToken(refreshToken);
        refreshTokenStore.setUserId(userId);
        return refreshTokenStore;
    }
}
