package com.ngariful.authentication.security;

import com.ngariful.authentication.model.User;
import com.ngariful.authentication.security.jwt.JwtUser;
import com.ngariful.authentication.security.jwt.JwtUserFactory;
import com.ngariful.authentication.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {
    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUsername(username).orElseThrow(() ->
                new UsernameNotFoundException( String.format("authentication with username: %s not found", username)));


        JwtUser jwtUser = JwtUserFactory.create(user);
        log.info("user with username: {} successfully loaded", username);
        return jwtUser;
    }
}