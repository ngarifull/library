package com.ngariful.authentication.controller;

import com.ngariful.authentication.model.dto.AuthenticationRequestDto;
import com.ngariful.authentication.model.dto.RefreshTokenRequestDto;
import com.ngariful.authentication.model.dto.TokenResponseDto;
import com.ngariful.authentication.service.AuthenticationService;
import com.ngariful.constants.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = Constants.API_BASE + "/auth")
public class AuthenticationRestController {
    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseEntity<TokenResponseDto> login(@RequestBody AuthenticationRequestDto requestDto) throws Exception {
        TokenResponseDto response = authenticationService.login(requestDto);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/refresh")
    public ResponseEntity<TokenResponseDto> refresh(@RequestBody RefreshTokenRequestDto data) throws Exception {
        TokenResponseDto response = authenticationService.refresh(data);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/login")
    public String loginTest(){
        return "hi!";
    }

    @GetMapping("/login2")
    public String loginTest2(){
        return "hi!";
    }
}