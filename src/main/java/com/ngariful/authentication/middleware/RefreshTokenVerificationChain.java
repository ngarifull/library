package com.ngariful.authentication.middleware;

import com.ngariful.authentication.middleware.refresh.token.RefreshTokenMiddleware;
import com.ngariful.authentication.middleware.refresh.token.impl.RefreshTokenExistsMiddleware;
import com.ngariful.authentication.model.dto.RefreshTokenRequestDto;
import com.ngariful.authentication.middleware.refresh.token.impl.RefreshTokenArchiveMiddleware;
import com.ngariful.authentication.middleware.refresh.token.impl.RefreshTokenExpiresMiddleware;
import com.ngariful.authentication.middleware.refresh.token.impl.RefreshTokenUserStatusMiddleware;
import com.ngariful.authentication.service.RefreshTokenStoreService;
import com.ngariful.authentication.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@RequiredArgsConstructor
@Component
public class RefreshTokenVerificationChain {
    private final UserService userService;
    private final RefreshTokenStoreService refreshTokenStoreService;

    private RefreshTokenMiddleware refreshTokenMiddleware;

    @PostConstruct
    private void init(){
        refreshTokenMiddleware = new RefreshTokenExistsMiddleware(refreshTokenStoreService, userService);
        refreshTokenMiddleware
                .linkWith(new RefreshTokenExpiresMiddleware())
                .linkWith(new RefreshTokenUserStatusMiddleware(userService))
                .linkWith(new RefreshTokenArchiveMiddleware(refreshTokenStoreService));
    }

    public boolean apply(RefreshTokenRequestDto refreshTokenRequest) throws Exception {
        return refreshTokenMiddleware.check(refreshTokenRequest);
    }
}
