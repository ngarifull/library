package com.ngariful.authentication.middleware.refresh.token;

public abstract class RefreshTokenMiddleware {
    private RefreshTokenMiddleware next;

    public RefreshTokenMiddleware linkWith(RefreshTokenMiddleware next) {
        this.next = next;
        return next;
    }

    public abstract <T> boolean check(T a) throws Exception;


    protected <T> boolean checkNext(T a) throws Exception {
        if (next == null) {
            return true;
        }
        return next.check(a);
    }
}
