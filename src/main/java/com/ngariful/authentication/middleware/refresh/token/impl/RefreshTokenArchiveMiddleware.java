package com.ngariful.authentication.middleware.refresh.token.impl;

import com.ngariful.authentication.middleware.refresh.token.RefreshTokenMiddleware;
import com.ngariful.authentication.model.RefreshTokenStore;
import com.ngariful.authentication.service.RefreshTokenStoreService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RefreshTokenArchiveMiddleware extends RefreshTokenMiddleware {
    private final RefreshTokenStoreService refreshTokenStoreService;

    @Override
    public <T> boolean check(T a) {
        RefreshTokenStore refreshTokenStore = (RefreshTokenStore) a;
        refreshTokenStoreService.archive(refreshTokenStore);
        return true;
    }
}
