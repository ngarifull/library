package com.ngariful.authentication.middleware.refresh.token.impl;

import com.ngariful.authentication.middleware.refresh.token.RefreshTokenMiddleware;
import com.ngariful.authentication.model.RefreshTokenStore;

import java.time.LocalDateTime;

public class RefreshTokenExpiresMiddleware extends RefreshTokenMiddleware {

    @Override
    public <T> boolean check(T a) throws Exception {
        RefreshTokenStore refreshTokenStore = (RefreshTokenStore) a;
        if (refreshTokenStore.getExpires().isAfter(LocalDateTime.now())) {
            return checkNext(refreshTokenStore);
        }

        throw new Exception("refractor me");
    }
}
