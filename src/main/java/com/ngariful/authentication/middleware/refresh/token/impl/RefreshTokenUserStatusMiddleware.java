package com.ngariful.authentication.middleware.refresh.token.impl;

import com.ngariful.authentication.middleware.refresh.token.RefreshTokenMiddleware;
import com.ngariful.authentication.model.RefreshTokenStore;
import com.ngariful.authentication.model.User;
import com.ngariful.authentication.model.UserStatus;
import com.ngariful.authentication.service.UserService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RefreshTokenUserStatusMiddleware extends RefreshTokenMiddleware {
    private final UserService userService;

    @Override
    public <T> boolean check(T a) throws Exception {
        RefreshTokenStore refreshTokenStore = (RefreshTokenStore) a;
        User user = userService.findById(refreshTokenStore.getUserId());
        if (UserStatus.isDisabled(user.getStatus())) {
            throw new Exception("refractor me");
        } else {
            return checkNext(refreshTokenStore);
        }
    }
}
