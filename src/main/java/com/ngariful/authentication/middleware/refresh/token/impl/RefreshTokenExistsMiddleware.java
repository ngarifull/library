package com.ngariful.authentication.middleware.refresh.token.impl;

import com.ngariful.authentication.middleware.refresh.token.RefreshTokenMiddleware;
import com.ngariful.authentication.model.RefreshTokenStore;
import com.ngariful.authentication.model.User;
import com.ngariful.authentication.model.dto.RefreshTokenRequestDto;
import com.ngariful.authentication.service.RefreshTokenStoreService;
import com.ngariful.authentication.service.UserService;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class RefreshTokenExistsMiddleware extends RefreshTokenMiddleware {
    private final RefreshTokenStoreService refreshTokenStoreService;
    private final UserService userService;

    @Override
    public <T> boolean check(T a) throws Exception {
        RefreshTokenRequestDto refreshToken = (RefreshTokenRequestDto) a;

        RefreshTokenStore refreshTokenStore = refreshTokenStoreService.get(refreshToken.getRefreshToken());
        if (refreshTokenStore == null) {
            throw new Exception("refractor me");
        }

        User user = userService.findById(refreshTokenStore.getUserId());
        if (!refreshToken.getUsername().equals(user.getUsername())) {
            throw new Exception("refractor me");
        }

        return checkNext(refreshTokenStore);
    }
}
