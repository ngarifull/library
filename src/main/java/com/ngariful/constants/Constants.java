package com.ngariful.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {
    public static final Byte TRUE = 1;
    public static final Byte FALSE = 0;

    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";

    public static final String API_BASE = "/api/v1/";
    public static final String ADMIN_ENDPOINT = String.format("%s/admin/**", Constants.API_BASE);
    public static final String LOGIN_ENDPOINT = String.format("%s/auth/login", Constants.API_BASE);
    public static final String PUBLIC_KEY_ENDPOINT = String.format("%s/public-key", Constants.API_BASE);
    public static final String BASIC_ENDPOINT = String.format("%s/**", Constants.API_BASE);

}
